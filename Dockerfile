FROM mcr.microsoft.com/dotnet/runtime:3.1 AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build
WORKDIR /src
COPY ["DockerTestProject.csproj", "."]
RUN dotnet restore "./DockerTestProject.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet publish -r win-x64 -c Release --self-contained -o /app/build

FROM build AS publish
RUN dotnet publish  -r win-x64 "DockerTestProject.csproj" -c Release -o /app/publish --self-contained
 
FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "DockerTestProject.dll"]
